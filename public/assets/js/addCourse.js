//console.log("hello from addCourse.js");

let createCourseForm = document.querySelector("#createCourse")

createCourseForm.addEventListener("submit", (e) => {
	e.preventDefault() //this is to avoid page refresh redirection once the event has been triggered.

	//capturing each values inside the input fields.
	let name = document.querySelector("#courseName").value //the ".value" describes the value attribute of the HTML element.
	console.log(name)	
	let cost = document.querySelector("#coursePrice").value
	console.log(cost)
	let desc = document.querySelector("#courseDescription").value
	console.log(desc)

	if (name === "" || cost === "" || desc === "") {
		Swal.fire({
			icon: 'error',
			title: 'Whoops!',
			text: 'Something went wrong. Check your input and make sure not to leave out blank entry fields.'
		})
	} else{
		//check if course exists.
		fetch("https://agile-plains-53286.herokuapp.com/api/courses/course-exists", {
			method: "POST",
			headers: {
				"Content-type": "application/json"
			} ,
			body: JSON.stringify({
				name
			})
		}).then(res => {
			return res.json();
		}).then(data => {
			console.log(data);
			if (data === true) {
				Swal.fire({
						icon: 'error',
						title: 'Whoops!',
						text: 'Course already exists.'
						})
			} else{
				fetch('https://agile-plains-53286.herokuapp.com/api/courses/addCourse', {
					method: 'POST',
					headers: {
						'Content-type': 'application/json'
					},	
					body: JSON.stringify({
						name: name,
						description: desc,
						price: cost
					}) //this section describes the body of the request, converted into a JSON format.
					//after describing the structure of the request body, now create the structure of the response coming from the back-end.
					}).then(res => {
						return res.json()
					}).then(data => {
						console.log(data)
						if(data === true){
							Swal.fire({
									icon: 'success',
									title: 'SUCCESS!',
									text: 'New course successfully added to the course database.'
							})
						
						} else {
							Swal.fire({
									icon: 'error',
									title: 'Whoops!',
									text: 'Something went wrong in the course creation.'
							})
							
						}
						document.querySelector("#courseName").value = "";
						document.querySelector("#coursePrice").value = "";
						document.querySelector("#courseDescription").value = "";
					})
			}
		})
	}
})