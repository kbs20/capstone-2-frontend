console.log("hello from JS file"); 

let registerForm = document.querySelector('#registerUser')

registerForm.addEventListener('submit', (event) => {
	event.preventDefault() //to avoid page refresh/redirection once that the event has been triggered.

	//first, capture each values inside the input fields.
	let firstName = document.querySelector("#firstName").value
	console.log(firstName)
	let lastName = document.querySelector("#lastName").value
	console.log(lastName)
	let userEmail = document.querySelector("#userEmail").value
	console.log(userEmail)
	let mobileNo = document.querySelector("#mobileNumber").value
	console.log(mobileNo)
	let password = document.querySelector("#password1").value
	console.log(password)
	let verifyPassword = document.querySelector("#password2").value
	console.log(verifyPassword)

	//information validation upon creating a new entry in the database.
	//let's create a control structure.
	//=> to check if passwords match.
	//=> to check if passwords are not empty.
	//=> to check the validation for mobile Number. What we can do  is to check the length of the mobile number input.
	if ((password !== "" && verifyPassword !== "") && (verifyPassword === password) && (mobileNo.length === 11)) {
		fetch('https://agile-plains-53286.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: userEmail
			})
		}).then(res => res.json()).then(data => {
			//this will give the information if there are no duplicates.
			if(data === false) {
				fetch('https://agile-plains-53286.herokuapp.com/api/users/register', {
					method: 'POST',
					headers: {
						'content-type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: userEmail,
						mobileNo: mobileNo,
						password: password
					}) //this section describes the body of the request converted into a JSON format.
				}).then(res => {
					return res.json()
				}).then(data => {
					console.log(data)
					if(data === true){
						Swal.fire({
							icon: 'success',
							title: 'SUCCESS!',
							text: 'New account registered successfully.'
							})
					} else{
						Swal.fire({
							icon: 'error',
							title: 'Whoops!',
							text: 'Something went wrong in the registration.'
							})
					}
				})
			}else {
				Swal.fire({
							icon: 'error',
							title: 'Whoops!',
							text: 'Email already exists. Choose another email.'
							})
			}
		})
	} else {
		Swal.fire({
			icon: 'error',
			title: 'Whoops!', //this is the alert message that pops up when the sweet alert appears.
			text: 'There is an empty data field. Please fill-in the empty field(s) with the desired data.'//can be used to display/show more details/info about the action/response. 
		});
	}
})


/*--- 02-24-2021 Individual activity ----*/
//Include the SweetAlert extensions on the following modules: addCourse, Register, Login
//change all the default alert() into Swal.
//you can be creative in modifying the content of the messages in the popup boxes.
//required elements: icon, title, text, and another key.