console.log('hello from course.js')

/*---(02-11-2021) start---*/

//The first thing that we need to do is to identify which course it needs to display inside the browser.
//We are going to use the course id to identify the correct course properly.
let params = new URLSearchParams(window.location.search)
//window.location ---> returns a location object with information about the current location of the document.
//.search --> contains the query string section of the current URL; search property returns an object of type stringString.
//URLSearchParams() --> this method/constructor creates and returns a URLSearchParams Object. (this is a class)
//"URLSearchParams" --> this describes the interface that defines utility methods to work with the query string of a URL. (this is a property)

let id = params.get('courseId')
console.log(id)

//(02-26-2021) let's capture the access token from the local storage.
let token = localStorage.getItem('token')
console.log(token)//this is added only for educational purposes.

//let's capture the sections of the html body.
let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")
let enroll = document.querySelector("#enrollmentContainer")//(02-26-2021)

fetch(`http://localhost:4000/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data)
	name.innerHTML = data.name
	desc.innerHTML = data.description
	price.innerHTML = data.price
	enroll.innerHTML = `<a id="enrollButton class="btn btn-success text-white btn-block">Enroll</a>`//(02-26-2021) what we did here is we inserted the enroll button component inside this page.

	//id="enrollButton"
	/*---(03-01-2021)Group Activity solution for enroll feature by Sir Marty (Start)---*/
	//we have to capture first the anchor tag for enroll, and an event listener to trigger an event.Then create a function in the eventListener() to describe the next set of procedures.
	document.querySelector("#enrollButton").addEventListener("click", () => {
		//insert the course to our enrollment array inside the user collection.
		fetch('http://localhost:4000/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}` //we have to get the value of the auth string to verufy and validate the user.
			},
			body: JSON.stringify({
				courseId: id
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			//now we can inform the user that the request has been done or not.
			if (data === true) {
				Swal.fire({
					icon: "success",
					title: "ENROLLED!",
					text: "Thank you for enrolling to this course."
				})
				window.location.replace("./courses.html")
			} else{
				//inform the user that the request has failed.
				Swal.fire({
					icon: "error",
					title: "ERROR!",
					text: "Something went wrong."
				})
			}
		})
	})
/*---(03-01-2021)Group Activity solution for enroll feature by Sir Marty (End)---*/



})//if the result is displayed in the console, it means you were able to properly pass the courseid and successfully created your fetch request.

/*---(02-11-2021) end---*/

/*---(02-26-2021)Group Activity solution for enroll feature (Start)---*/
/*enroll.addEventListener('click', (e) => {
	e.preventDefault()
	
	fetch(`http://localhost:4000/api/users/enroll`, {
		method: 'POST',
		headers: { 
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}` 
		},
		body: JSON.stringify({
			userId: token,
			courseId: id 
		})
	}).then(res => {
		return res.json()
	}).then(data => {
		console.log(data)
		if (data === true) {
			Swal.fire({
					icon: 'success',
					title: 'SUCCESS!',
					text: 'Enrollment Successful'
					})
			//window.location.replace("./courses.html")
		} else {
			Swal.fire({
					icon: 'error',
					title: 'Whoops!',
					text: 'An error has been encountered during the enrollment process.'
					})
		}
	})
});*/
/*---(02-26-2021)Group Activity solution (End)---*/



//https://agile-plains-53286.herokuapp.com
