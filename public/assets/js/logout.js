//clear/wipeout all the data inside our local storage so that the session of the user will end.
localStorage.clear(); // the clear method will allow you to remove the contents of the storage object.
//after being cleared, redirect the user to the login page so that just in case a new user wants to login.
//window.location.replace('./login.html');