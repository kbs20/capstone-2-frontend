console.log("hello from deleteCourse.js");


let queryString = new URLSearchParams(window.location.search);//a "URLSearchParams" object that we can access specific parts of the query string. 

console.log(...queryString); //using the spread operator to set the key-value pairs of the object URLSearchParams.

console.log(queryString.has('courseId')); //using the has() method to check if the courseId string exists in the URL query string.

console.log(queryString.get('courseId')); //using the .get() method to check for the value of 'courseId'.

let courseId = queryString.get('courseId');

//console.log(courseId);




/*======================================================*/


//retrieving the token from the local storage:
let token = window.localStorage.getItem('token');
//console.log(token);

/*======================================================*/




//Creating a fetch request to get the proper course id:
fetch(`http://localhost:4000/api/courses/${courseId}`, 
	{
		method: 'DELETE',
		headers: {
			'Authorization': `Bearer ${token}`
		}

	}).then(res => {
		return res.json()

	}).then(data => { //creation of new course successful
		if(data === true) {
			//redirect to courses index page
			window.location.replace('./courses.html')
		} else {
			//error in creating course; redirect to error page
			alert('something went wrong')
		}

	})



/*===================  END =====================*/