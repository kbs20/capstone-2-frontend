/*------ (02-16-2021) Start ------*/
console.log("hello from login.js");

let loginForm = document.querySelector('#loginUser');


loginForm.addEventListener("submit", (e) => {
	e.preventDefault() //this prevents page redirection.

	let email = document.querySelector("#userEmail").value
	console.log(email)
	let password = document.querySelector("#password").value
	console.log(password)

	//how can we inform a user that a blank input field cannot be logged in?
	if(email == "" || password == "") {
		Swal.fire({
			icon: 'error',
			title: 'Missing Credentials!',
			text: 'Please complete all blank fields.'
		})
	} else{
		fetch('https://agile-plains-53286.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			console.log(data.access)
			if(data.access) {
				//let's save the access token inside our local storage.
				localStorage.setItem('token', data.access) //this local storage can be found inside the subfolder of the appData of the local file of the google chrome browser inside the data module of the user folder. ( \AppData\Local\Google\Chrome\User Data\Default\Local Storage)
				//alert("access key saved on local storage.")//these alert messages are only for educational purposes.
				fetch(`https://agile-plains-53286.herokuapp.com/api/users/details`, {
					headers: {
						'Authorization': `Bearer ${data.access}`
					}
				}).then(res => {
					return res.json()
				}).then(data => {
					//let's create a checker to see if may nakukuhang data.
					console.log(data)
					localStorage.setItem("id", data._id) //this came from the payload.
					localStorage.setItem("isAdmin", data.isAdmin)
					console.log("items are set inside the local storage.")
					//direct the user to the courses page upon successful login.
					window.location.replace('./courses.html')
				})
			} else{
				//if there is no existing access key value from the data variable then just inform the user.
				Swal.fire({
					icon: 'error',
					title: 'Credential Error!',
					text: 'Check your Username or Password.'
				})
			}
		})
	}


});

/*------ (02-16-2021) End ------*/